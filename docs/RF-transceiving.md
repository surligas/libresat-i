# RF transceiving
## Intro
Communication subsystem

## Scope
The COMMS subsustem is the primary communication channel with 
the sattelite. It is responsible for delivering telemetry and data to the ground
and accepting control commands.


## Assumptions
The COMMS subsystem uses a single-carrier narrowband transceiver at fixed uplink
and downlink frequencies.
Any other type of communication (multi-carrier, high-throughut, e.t.c) is
mission specific and considered as payload.

## Requirements
Detailed requirements with links and documentation-rationale

## Components

### Hardware

### Software

### Physical Layer (PHY)
The PHY of COMMS is based on the IEEE 802.15.4 standard (BPSK) with strong variations on the FEC used.
COMMS use BPSK 

#### Modulation
Theoretically BPSK and QPSK have the same BER performance measured in AWGN channel.
However, the Doppler effect of the LEO channel and the strong variations on temperature introduce large CFO.
For this reason COMMS uses BPSK which is more resilient to constellation rotation.

#### Forward Error Correction (FEC)
The IEEE 802.15.4 uses DSSS spreading chip sequences to deal with errors.
This scheme is very power efficient but the coding rate is quite high (1/15), thus the effective data rate is low.

For this reason, COMMS uses inner and outer FEC. For the inner FEC, a convolutional coder with K=7, R=1/2 is used
whereas for the outer a Reed-Solomon is used.
The FEC is assisted by a synchronous scrambler to further increase the performance.
To reduce the frame header overhead and the overall scrambling time, the initialization sequence for the synchronous scrambler is fixed.

All FEC operations are performed on the MCU. 
This may not be the most power efficient solution but provides the best flexibility without relying on a specific RF frontend.
FEC can be dynamically enabled/disabled to meet the mission requirements.

#### Synchronization
Synchronization is the most critical part of COMMS PHY. COMMS follows the IEEE 802.15.4 two phase frame synchronization.
A repeated training sequence **TS1** is sent, followed by a second unique training sequence **TS2**.
TS1 is used for initial frame detection, coarse synchronization and wake-on-radio if the RF frontend supports it, whereas TS2 for fine synchronization and start of frame detection.  

### Link Layer
#### Frame format
The frame format of COMMS is quite simple. After the TS2 sequence there is a 1 byte field that indicates the size of variable length payload.
Due to the header size the allowed payload sizes are 1-255 bytes. After reading the legth field of the header, 

Training Sequences | Header | Payload 
------------------ | ------ | -------
  32 * TS1 + TS2   |  Frame length (1 byte)   | Variable length (1 - 255 bytes)

For compatibility and legacy purposes the payload encapsulates an AX.25 frame, 
**without** the leading-trailing AX.25 synchronization flags and **without** bit stuffing, as all of them are redundant. 

### In-Sat communication API
The COMMS subsystem provides a general API to the user for sending and receiving data, as well as controlling some of the parameters of the RF frontend.
It totally abstracts all the underlined details of the telecommunication protocol and the data exchange/control with the RF frontend. 

COMMS provides a POSIX style API for sending and receiving data.
 * `comm_sock_t* comms_open()`:  Creates a COMMS socket, that can be used to send and receive data. 
 * `ssize_t comms_send(comm_sock_t *sockfd, const void *buf, size_t len, int flags)`: Sends `len` data bytes from buffer `buf`. Returns the number of bytes actually sent.
 * `ssize_t comms_recv(comm_sock_t *sockfd, void *buf, size_t len, int flags, size_t timeoutms)`: Receives at most `len` bytes and stores them at `buf`. Returns the number of bytes actually received.
 * `comms_close(comm_sock_t *sockfd)`

## Related Systems
Links to related systems if needed
## External Links
* [IEEE 802.15.4 standard] (https://standards.ieee.org/about/get/802/802.15.html)
