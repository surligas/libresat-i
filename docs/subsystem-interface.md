# Subsystem interface

## Intro
The subsystem interface is general-reusable component. Its purpose is
to connect all subsystems with a common communication bus.

## Scope
This component consists of electrical, physical (hardware) and software
sub-components. Electrical sub-components can be the I2C bus or
CAN bus, while physical means the way that boards (sub - systems) connects to each
other for example PC-104 with SAMTEC-ESQ-126-21-G-D. As for software, it can be described as
the application layer that could be specified from the electrical sub-component e.g
SpaceWire (LVDS).

## Assumptions
LibreSAT-i is based on a distributed architecture. For that reason it is necessary
to use a multi-master communication bus to provide each subsystem with the ability
to communicate with all other subsystems in the satellite.

It is also assumed that the communication bus will be isolated for avoidance of ground
loops and interferences. The isolation of each sub system could be done with a specific electrical
protocol like CAN bus or by using external isolation IC, within e.g. the UART
electrical protocol.

Other considerations include the speed of the electrical bus that is
mission specific and the redundancy of the bus that is mandated by the multi-master and the
distributed architecture.

Additionally it is assumed that the electrical interface selected would need to
be implemented on a micro controller or by an external specific IC.

The physical interface is ideally modular in order facilitate different connection
and interface types. Additionally, the interface should have the ability to
accommodate the electrical interface. For example, if the choice is a multi-master
I2C, the physical bus must have the ability to accommodate 4 buses for contingency.

It is desirable for the interface to be connected with existing COTS sub systems.

## Requirements
The summary of assumptions is listed is this section.

* Electrical interface:
 * Multi - master architecture
 * Isolated communication bus (for gnd loop avoidance and interferences)
 * Implemented by micro controller or external IC
 * Reduced overhead
 * Reliable, robust and tested in industry (space or automotive)
* Physical interface:
 * Modular
 * Scalable
 * Interfaces with existing COTS sub systems

## Components
List of components of the subsystem interface.
* Electrical interface
* Physical interface

## Related Systems
Pretty much with every other subsystem is related.
Mostly with [Structural](Mechanical.md) and [Command and Control]().

## External Links
List of links for further reading
